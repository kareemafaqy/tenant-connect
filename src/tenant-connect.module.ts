import {
  BadRequestException,
  DynamicModule,
  Inject,
  Logger,
  MiddlewareConsumer,
  Module,
  NestModule,
} from '@nestjs/common';
import { NextFunction, Request } from 'express';
import {
  Connection,
  ConnectionOptions,
  createConnection,
  getConnection,
} from 'typeorm';
import { ConfigModule, ConfigService } from '@nestjs/config';
import {
  ADMIN_SERVICE,
  TENANT_CONNECTION,
  TenantConnectProvider,
} from './tenant-connect.provider';
import { ClientProxy, ClientsModule, Transport } from '@nestjs/microservices';
import { lastValueFrom } from 'rxjs';
import { GetTenantDatabaseNameHelper } from '@afaqy/core';

@Module({
  imports: [
    ClientsModule.registerAsync([
      {
        name: ADMIN_SERVICE,
        imports: [ConfigModule],
        useFactory: async (configService: ConfigService) => ({
          transport: Transport.TCP,
          options: {
            port: configService.get<number>('adminMSPort'),
          },
        }),
        inject: [ConfigService],
      },
    ]),
  ],
  controllers: [],
  providers: [TenantConnectProvider],
  exports: [TENANT_CONNECTION],
})
export class TenantConnectModule implements NestModule {
  private static entities = [];

  constructor(
    private readonly connection: Connection,
    private readonly configService: ConfigService,
    @Inject(ADMIN_SERVICE) private readonly adminService: ClientProxy,
  ) {}

  static forFeature(entities: any[]): DynamicModule {
    this.entities = [...new Set([...this.entities, ...entities])];
    return {
      module: TenantConnectModule,
    };
  }

  configure(consumer: MiddlewareConsumer): void {
    consumer
      .apply(async (req: Request, res: Response, next: NextFunction) => {
        const name: string = req.headers.host.split('.')[0];
        let client: { subdomain: string };
        try {
          client = await lastValueFrom(
            this.adminService.send(
              { cmd: 'get_tenant_details' },
              { subdomain: name },
            ),
          );
        } catch (error) {
          Logger.log(
            `Error while trying to get Tenant details`,
            'TenantConnectModule',
          );
          throw new BadRequestException(
            `${name} - This tenant does not exists`,
          );
        }

        const tenantDatabaseName = GetTenantDatabaseNameHelper(
          this.configService.get<string>('environmentName'),
          client.subdomain,
        );

        try {
          // If tenant's connection is already exist
          getConnection(tenantDatabaseName);
          Logger.log(`Connection is ready`, `${tenantDatabaseName}`);
          next();
        } catch (e) {
          // Create DB if not exists
          await this.connection.query(
            `CREATE DATABASE IF NOT EXISTS ${tenantDatabaseName} CHARACTER SET utf8`,
          );
          const connectionOptions: ConnectionOptions = {
            type: 'mysql',
            host: this.configService.get<string>('database.host'),
            port: this.configService.get<number>('database.port'),
            username: this.configService.get<string>('database.username'),
            password: this.configService.get<string>('database.password'),
            name: tenantDatabaseName,
            database: tenantDatabaseName,
            entities: TenantConnectModule.entities,
            synchronize: false,
          } as ConnectionOptions;
          const createdConnection: Connection = await createConnection(
            connectionOptions,
          );
          // Once it "ready" move on :)
          if (createdConnection) {
            Logger.log(`connection created`, `${tenantDatabaseName}`);
            await createdConnection.runMigrations({ transaction: 'all' });
            next();
          } else {
            throw new BadRequestException(
              'Database Connection Error',
              'There is a Error with the Database!',
            );
          }
        }
      })
      .forRoutes('*');
  }
}
