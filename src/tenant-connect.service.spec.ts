import { Test, TestingModule } from '@nestjs/testing';
import { TenantConnectService } from './tenant-connect.service';

describe('TenantConnectService', () => {
  let service: TenantConnectService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [TenantConnectService],
    }).compile();

    service = module.get<TenantConnectService>(TenantConnectService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
