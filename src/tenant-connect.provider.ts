import { BadRequestException, Logger, Provider, Scope } from '@nestjs/common';
import { REQUEST } from '@nestjs/core';
import { getConnection } from 'typeorm';
import { Request } from 'express';
import { ClientProxy } from '@nestjs/microservices';
import { lastValueFrom } from 'rxjs';
import { GetTenantDatabaseNameHelper } from '@afaqy/core';
import { ConfigService } from '@nestjs/config';

export const TENANT_CONNECTION = 'TENANT_CONNECTION';
export const ADMIN_SERVICE = 'ADMIN_SERVICE';

export const TenantConnectProvider: Provider = {
  provide: TENANT_CONNECTION,
  inject: [REQUEST, ADMIN_SERVICE, ConfigService],
  scope: Scope.REQUEST,
  useFactory: async (
    req: Request,
    adminService: ClientProxy,
    configService: ConfigService,
  ) => {
    const name: string = req.headers.host.split('.')[0];
    let client: { subdomain: string };
    let clientDatabaseName: string;
    try {
      client = await lastValueFrom(
        adminService.send({ cmd: 'get_tenant_details' }, { subdomain: name }),
      );
      clientDatabaseName = GetTenantDatabaseNameHelper(
        configService.get<string>('environmentName'),
        client.subdomain,
      );
      Logger.log('TenantConnectProvider', clientDatabaseName);
    } catch (error) {
      Logger.log(
        `Error while trying to get Tenant details`,
        'TenantConnectProvider',
      );
      throw new BadRequestException('404 database not found');
    }
    return getConnection(clientDatabaseName);
  },
};
